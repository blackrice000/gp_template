#ifndef __LCD_H
#define __LCD_H	  

void ArmRccConfig(void);
void ArmNvicConfig(void);
void ArmGpioConfig(void);

#endif /* __LCD_H */
