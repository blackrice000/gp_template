#include "stm32f10x_lib.h"
#include "Uart.h"
#include "stdio.h"

void UART_CLOCK_CONFIG(void);
void UART_FORMAT_CONFIG(void);
void UART_GPIO_CONFIG(void);

void Uart1Config(void){
//	UART_CLOCK_CONFIG();
	UART_GPIO_CONFIG();
	UART_FORMAT_CONFIG();
	USART_Cmd(USART1, ENABLE);
}



void UART_CLOCK_CONFIG(void){
	USART_ClockInitTypeDef  USART_ClockInitStructure;

	USART_ClockInitStructure.USART_Clock = USART_Clock_Disable;
	USART_ClockInitStructure.USART_CPOL = USART_CPOL_Low;
	USART_ClockInitStructure.USART_CPHA = USART_CPHA_2Edge;
	USART_ClockInitStructure.USART_LastBit = USART_LastBit_Disable;
	USART_ClockInit(USART1, &USART_ClockInitStructure);
}




void UART_FORMAT_CONFIG(void){
	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
}




void UART_GPIO_CONFIG(void){
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(
	RCC_APB2Periph_GPIOA| 
	RCC_APB2Periph_AFIO|
	RCC_APB2Periph_USART1, ENABLE);


	
	//Set Uart
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //TX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//RX
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	//end Set Uart
}




//���s�w�q fputc
int fputc(int ch, FILE *f){
	USART_SendData(USART1, (unsigned char) ch);
	while (!(USART1->SR & USART_FLAG_TXE));
	return (ch);
}

int GetKey (void)  {
	while (!(USART1->SR & USART_FLAG_RXNE));
	return ((int)(USART1->DR & 0x1FF));
}
