#include "stm32f10x_lib.h"
#include "ArmSet.h"


void ArmRccConfig(void){
	ErrorStatus HSEStartUpStatus;

	RCC_DeInit();
	RCC_HSEConfig(RCC_HSE_ON);
	HSEStartUpStatus = RCC_WaitForHSEStartUp();
	
	if(HSEStartUpStatus == SUCCESS){
    	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
		FLASH_SetLatency(FLASH_Latency_2);
		RCC_HCLKConfig(RCC_SYSCLK_Div1); 
		RCC_PCLK2Config(RCC_HCLK_Div1); 
		RCC_PCLK1Config(RCC_HCLK_Div2);
		RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);
		RCC_PLLCmd(ENABLE);

    		while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET){
    		}
    	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

    		while(RCC_GetSYSCLKSource() != 0x08){
		    }
		}
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);
}




void ArmNvicConfig(void){
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

//  NVIC_InitStructure.NVIC_IRQChannel = SDIO_IRQChannel;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQChannel;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	#ifdef  VECT_TAB_RAM  
	NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0); 
	#else
	NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);   
	#endif
}




void ArmGpioConfig(void){
	GPIO_InitTypeDef GPIO_InitStructure; 
	RCC_APB2PeriphClockCmd(
		RCC_APB2Periph_GPIOC|
		RCC_APB2Periph_GPIOA, ENABLE); 

	//Set push button 
	GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_6;	//PB6
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);  				  

	GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_7|GPIO_Pin_8; //PC7, PC8
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	//end Set push button  

}
